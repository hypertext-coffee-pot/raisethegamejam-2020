# HyperText Coffee Pot ftw!
## Team HTCP's submission for raisethegamejam2020

I hope everything goes well!


# GitLab Instructions!
## First Time Setup
#### 1. Please **copy (CTRL+C)** the 'HTTPS' URL for [this project](https://gitlab.com/hypertext-coffee-pot/raisethegamejam-2020). 
![image](/uploads/d1bed15fe46a73658ea9ef338aa8025a/image.png)
#### 2. In GitKraken, **clone** the project to your machine somewhere! 
![image](/uploads/c71ae1ca4290079c634270997492c494/image.png)
#### 3. **Go to** that cloned folder, this is where you will make your changes in UE4 or whatever program you'd use!
![image](/uploads/4f7a6655b469beb83822ffff3ca16fee/image.png)

## How to Make Changes
#### 1. Make sure you press 'Pull' on the master branch in GitKraken to make sure it's up to date.
![image](/uploads/6104572486d3eb0169e786daf1caca44/image.png)
#### 2. Click `Branch` near the top-middle of GitKraken. Type in a name for what you'll be working on, such as 'textures' or 'sound-design'.
![image](/uploads/9d02cfc4403390f1215a895d9a8e3adf/image.png)
![image](/uploads/4ca0c268f9b4a7db9a96f7d26eaa9ca0/image.png)
#### 3. Make your changes...
#### 4. Once you've made your changes, you should be able to see them under '**Unstaged Changes**'. Simply press `Stage all changes`.
![image](/uploads/2aad0eea2ed6bfee6159e8da64e76fc0/image.png)
#### 5. Give 'em a name, click `Commit changes` and then finally you can click `Push`. This saves a 'snapshot' of everything you've done and it's a good idea to make very regular commits, about as often as you'd save your work.
![image](/uploads/4ebb859b510db1d214ec5a22f8f67c2b/image.png)
![image](/uploads/18a3e05d56ff9bcacbf416ba32432890/image.png)
![image](/uploads/31a92baef56c4f163a8e28f9833b6ec5/image.png)
![image](/uploads/df1a69876e7540fcca406d8e6748a135/image.png)
If you see the above message like so: `What remote/branch should "monster-munch" push to and pull from? [origin/monster-munch]`,
just click on '**Submit**' and don't change anything.

If instead, you get an _**ANGRY RED ERROR**_ in the bottom-left hand corner, make sure you created a branch in step 2.
#### 6. Finally, [click here](https://gitlab.com/hypertext-coffee-pot/raisethegamejam-2020/-/branches) and click 'Merge Request' to be able to **share your changes with the group**.
![image](/uploads/5a41c1ed5d5c624a29d73f9e8e0e7547/image.png)
_(The 1 means I have made one commit that I can merge.)_
#### 6a. If you'd like to make more changes after making your merge request, repeat from step 4. Don't reuse the same branch as earlier, always make a new one!